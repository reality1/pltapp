# in the name of ALLAH
# programmers: Ali Salimi and Amin Izadi
# name: PLT Application

from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
from PIL import Image, ImageTk
import numpy as np
import json
import cv2
import pydicom
import glob
import queue
import magic
import copy
import timeit
from collections import defaultdict


class ImageProcessing:

    def __init__(self):
        return

    def readDicom(self, path):
        files = glob.glob(path + '/*')

        slices = 0
        rows = 0
        columns = 0
        for file in files:
            if magic.from_file(file, mime=True) == 'application/dicom':
                first_file = pydicom.read_file(file)
                rows = first_file.Rows
                columns = first_file.Columns
                slices = first_file.NumberOfSlices
                break

        dicom = np.empty((slices, rows, columns))
        for file in files:
            if magic.from_file(file, mime=True) == 'application/dicom':
                slice_ = pydicom.dcmread(file)
                instance_num = slice_.InstanceNumber - 1
                pixels = slice_.pixel_array
                dicom[instance_num] = pixels

        dicom_max = np.amax(dicom)
        dicom_min = np.amin(dicom)
        dicom_size = dicom.shape
        return dicom, (dicom_min, dicom_max), tuple(dicom_size)

    def binarization(self, volume, volume_bounds, threshold):
        threshold = threshold * (volume_bounds[1] - volume_bounds[0]) + volume_bounds[0]
        barray = (volume > threshold) * 1
        # StartTime = timeit.default_timer()
        # result = np.where(barray != 0)
        # coordinates = zip(result[0], result[1],result[2])
        # StopTime = timeit.default_timer()
        # print(StopTime - StartTime)
        return barray

    def Growth(self, p, data, segment, Q):
        x = p[0]
        y = p[1]
        z = p[2]
        nts = [
            (x - 1, y - 1, z - 1),
            (x - 1, y - 1, z),
            (x - 1, y - 1, z + 1),
            (x - 1, y, z - 1),
            (x - 1, y, z),
            (x - 1, y, z + 1),
            (x - 1, y + 1, z - 1),
            (x - 1, y + 1, z),
            (x - 1, y + 1, z + 1),
            (x, y - 1, z - 1),
            (x, y - 1, z),
            (x, y - 1, z + 1),
            (x, y, z - 1),
            (x, y, z + 1),
            (x, y + 1, z - 1),
            (x, y + 1, z),
            (x, y + 1, z + 1),
            (x + 1, y - 1, z - 1),
            (x + 1, y - 1, z),
            (x + 1, y - 1, z + 1),
            (x + 1, y, z - 1),
            (x + 1, y, z),
            (x + 1, y, z + 1),
            (x + 1, y + 1, z - 1),
            (x + 1, y + 1, z),
            (x + 1, y + 1, z + 1)
        ]

        for t in nts:
            try:
                if data[t[0]][t[1]][t[2]] == 1:
                    data[t[0]][t[1]][t[2]] = 0
                    # Labels[t] = LSL
                    segment.add(t)
                    Q.put(t)
            except:
                continue

    def GrowthC(self, p, data, segment, Q):
        x = p[0]
        y = p[1]
        z = p[2]
        nts = [
            (x - 1, y - 1, z - 1),
            (x - 1, y - 1, z),
            (x - 1, y - 1, z + 1),
            (x - 1, y, z - 1),
            (x - 1, y, z),
            (x - 1, y, z + 1),
            (x - 1, y + 1, z - 1),
            (x - 1, y + 1, z),
            (x - 1, y + 1, z + 1),
            (x, y - 1, z - 1),
            (x, y - 1, z),
            (x, y - 1, z + 1),
            (x, y, z - 1),
            (x, y, z + 1),
            (x, y + 1, z - 1),
            (x, y + 1, z),
            (x, y + 1, z + 1),
            (x + 1, y - 1, z - 1),
            (x + 1, y - 1, z),
            (x + 1, y - 1, z + 1),
            (x + 1, y, z - 1),
            (x + 1, y, z),
            (x + 1, y, z + 1),
            (x + 1, y + 1, z - 1),
            (x + 1, y + 1, z),
            (x + 1, y + 1, z + 1)
        ]

        for t in nts:
            if t in data:
                np = data[t]
                del data[t]
                # Labels[t] = LSL
                segment.add(t)
                Q.put(np)

    def pointGrowth(self, BArray, voxel):
        # Data Structures

        segment = set()
        Q = queue.Queue()

        segment.add(voxel)
        Q.put(voxel)
        barray = copy.copy(BArray)
        barray[voxel[0]][voxel[1]][voxel[2]] = 0
        while not Q.empty():
            p = Q.get()
            self.Growth(p, barray, segment, Q)
        return segment

    def pointGrowthC(self, coordinates, voxel):
        # Data Structures
        Dataset = {}
        segment = set()
        Q = queue.Queue()

        # Dataset initialization
        for c in coordinates:
            Dataset[c] = c


        # Start Segmentation
        segment.add(voxel)
        Q.put(voxel)
        del Dataset[voxel]
        while not Q.empty():
            p = Q.get()
            self.GrowthC(p, Dataset, segment, Q)
        return segment


class Dicom:

    def __init__(self):
        self.path = None
        self.dicom = None
        self.dicom_bounds = None
        self.dicom_size = None
        self.buffer_binary = None
        self.current_threshold = None
        self.current_view = None
        self.current_type = None
        self.current_slice = None
        self.anomal_voxels = None
        self.ip = None
        self.threshold_volume = None
        return

    def changeThreshold(self, threshold):
        if threshold >= 0 and threshold <= 1:
            self.current_threshold = threshold
            self.buffer_binary = self.ip.binarization(self.dicom, self.dicom_bounds, self.current_threshold)
        return self.refreshFrame()

    def changeView(self, view):
        self.current_view = view
        self.current_slice = 0
        return self.refreshFrame()

    def changeType(self, type_):
        self.current_type = type_
        return self.refreshFrame()

    def changeColor(self, color):
        self.current_color = color
        return self.refreshFrame()

    def changeSlice(self, direction):
        if direction == 'next':
            if self.current_view == 'axial' and self.current_slice < self.dicom_size[0] - 1:
                self.current_slice += 1
            elif self.current_view == 'coronal' and self.current_slice < self.dicom_size[1] - 1:
                self.current_slice += 1
            elif self.current_view == 'sagital' and self.current_slice < self.dicom_size[2] - 1:
                self.current_slice += 1
        elif direction == 'back':
            if self.current_slice > 0:
                self.current_slice -= 1
        elif type(direction) == int:
            self.current_slice = direction
            if self.current_view == 'axial' and self.current_slice > self.dicom_size[0] - 1:
                self.current_slice = self.dicom_size[0] - 1
            elif self.current_view == 'coronal' and self.current_slice > self.dicom_size[1] - 1:
                self.current_slice = self.dicom_size[1] - 1
            elif self.current_view == 'sagital' and self.current_slice > self.dicom_size[2] - 1:
                self.current_slice = self.dicom_size[2] - 1
            elif self.current_slice < 0:
                self.current_slice = 0
        return self.refreshFrame()

    def refreshFrame(self):
        if self.current_type == 'dicom':
            dicom = self.dicom
            volume = self.rotation(dicom)
            slice_ = volume[self.current_slice]
            slice_max = np.max(slice_)
            slice_min = np.min(slice_)
            if self.current_color == 'inverse':
                if (slice_max - slice_min) == 0:
                    slice_[:] = 1
                else:
                    slice_ = 1 - ((slice_ - slice_min) / (slice_max - slice_min))
            elif self.current_color == 'original':
                if (slice_max - slice_min) == 0:
                    slice_[:] = 0
                else:
                    slice_ = ((slice_ - slice_min) / (slice_max - slice_min))
            frame = np.stack((slice_, slice_, slice_), axis=2)
        elif self.current_type == 'binary':
            binary = self.buffer_binary
            volume = self.rotation(binary)
            slice_ = volume[self.current_slice]
            slice_shape = slice_.shape
            frame = np.zeros((slice_shape[0], slice_shape[1], 3))
            for i in range(0, slice_shape[0]):
                for j in range(0, slice_shape[1]):
                    original_coordinate = self.getOriginalCoord((self.current_slice, i, j))
                    if slice_[i][j] == 0:
                        if original_coordinate not in self.anomal_voxels:
                            pass
                        else:
                            frame[i][j] = [1, 1, 0]
                    else:
                        if original_coordinate not in self.anomal_voxels:
                            frame[i][j] = [1, 1, 1]
                        else:
                            frame[i][j] = [1, 0, 0]
        elif self.current_type == 'mask':
            dicom = self.dicom
            volume = self.rotation(dicom)
            slice_ = volume[self.current_slice]
            slice_max = np.max(slice_)
            slice_min = np.min(slice_)
            if self.current_color == 'inverse':
                slice_ = 1 - ((slice_ - slice_min) / (slice_max - slice_min))
            elif self.current_color == 'original':
                slice_ = ((slice_ - slice_min) / (slice_max - slice_min))
            frame = np.stack((slice_, slice_, slice_), axis=2)
            binary = self.buffer_binary
            binary_volume = self.rotation(binary)
            binary_slice = binary_volume[self.current_slice]
            slice_shape = binary_slice.shape
            for i in range(0, slice_shape[0]):
                for j in range(0, slice_shape[1]):
                    original_coordinate = self.getOriginalCoord((self.current_slice, i, j))
                    if binary_slice[i][j] == 0:
                        if original_coordinate not in self.anomal_voxels:
                            pass
                        else:
                            frame[i][j] = [1, 1, 0]
                    else:
                        if original_coordinate not in self.anomal_voxels:
                            frame[i][j] = [0, 1, 0]
                        else:
                            frame[i][j] = [1, 0, 0]
        return frame

    def rotation(self, volume):
        if self.current_view == 'coronal':
            result = np.rot90(volume, 1, (0, 1))
        elif self.current_view == 'sagital':
            result = np.rot90(np.rot90(volume, 1, (2, 0)), 1, (1, 2))
        else:
            result = volume
        return result

    def getOriginalCoord(self, coordinate):
        if self.current_view == 'coronal':
            original_coordinate = (coordinate[1], self.dicom_size[1] - (coordinate[0] + 1), coordinate[2])
        elif self.current_view == 'sagital':
            original_coordinate = (coordinate[1], coordinate[2], coordinate[0])
        else:
            original_coordinate = tuple(coordinate)
        return original_coordinate

    def selectSegment(self, coordinate):
        original_coordinate = self.getOriginalCoord(coordinate)
        coord_value = self.buffer_binary[original_coordinate[0]][original_coordinate[1]][original_coordinate[2]]

        if coord_value == 0:
            if original_coordinate not in self.anomal_voxels:
                pass
            else:
                segmented_voxels = self.ip.pointGrowthC(self.anomal_voxels, original_coordinate)
                for sv in segmented_voxels:
                    if self.buffer_binary[sv[0]][sv[1]][sv[2]] == 0:
                        self.anomal_voxels.remove(sv)
                        self.threshold_volume[sv[0]][sv[1]][sv[2]] = -1
                    else:
                        self.threshold_volume[sv[0]][sv[1]][sv[2]] = self.current_threshold
        else:
            if original_coordinate not in self.anomal_voxels:
                segmented_voxels = self.ip.pointGrowth(self.buffer_binary, original_coordinate)
                self.anomal_voxels.update(segmented_voxels)
                indices = np.array(list(segmented_voxels))
                indices = indices.transpose()
                self.threshold_volume[tuple(indices)] = self.current_threshold
            else:
                segmented_voxels = self.ip.pointGrowthC(self.anomal_voxels, original_coordinate)
                self.anomal_voxels = self.anomal_voxels.difference(segmented_voxels)
                indices = np.array(list(segmented_voxels))
                indices = indices.transpose()
                self.threshold_volume[tuple(indices)] = -1
        return self.refreshFrame()

    def save(self):
        temp = list()
        for av in self.anomal_voxels:
            temp.append(list(av))
        with open(self.path + '/anomal_voxels.json', 'w') as f:
            json.dump(temp, f)

        indices = np.where(self.threshold_volume > 0)
        values = self.threshold_volume[indices]
        indices = np.array(indices).transpose()
        results = defaultdict(list)

        for i, j in zip(values, indices):
            results[str(i)].append(j.tolist())

        with open(self.path + '/anomal_voxels_threshold.json', 'w') as f:
            json.dump(results, f)
        return

    def open(self, path):
        self.path = path
        self.ip = ImageProcessing()
        (self.dicom, self.dicom_bounds, self.dicom_size) = self.ip.readDicom(self.path)
        self.current_threshold = 0
        self.buffer_binary = self.ip.binarization(self.dicom, self.dicom_bounds, self.current_threshold)
        self.current_view = 'axial'
        self.current_type = 'dicom'
        self.current_color = 'original'
        self.current_slice = 0
        self.anomal_voxels = set()
        self.threshold_volume = np.zeros(self.dicom_size)
        self.threshold_volume = self.threshold_volume - 1
        try:
            with open(self.path + '/anomal_voxels.json', 'r') as f:
                temp = json.load(f)
                for t in temp:
                    self.anomal_voxels.add(tuple(t))
        except:
            pass

        try:
            with open(self.path + '/anomal_voxels_threshold.json', 'r') as f:
                temp = json.load(f)
                for key in temp:
                    for v in temp[key]:
                        self.threshold_volume[v[0],v[1],v[2]] = float(key)
        except:
            pass
        return self.refreshFrame()

    def getInfo(self):
        string = ''
        return string


class PltApp:
    def __init__(self, master):
        self.file_path = str()
        self.dicom = Dicom()
        self.slice_number = IntVar()
        self.image_view = StringVar()
        self.image_type = StringVar()
        self.image_color = StringVar()
        self.image_threshold = DoubleVar()
        self.zoom_level = 1.0
        self.last_output = None
        self.state = None

        master.title('PLT Application')
        master.geometry('900x480+200+200')

        self.panedwindow = ttk.Panedwindow(master, orient=HORIZONTAL)
        self.frame_desk = ttk.Frame(master, relief=SUNKEN)
        self.frame_info = ttk.Frame(master, relief=SUNKEN)
        self.panedwindow.pack(fill=BOTH, expand=True)
        self.panedwindow.add(self.frame_desk, weight=4)
        self.panedwindow.add(self.frame_info, weight=1)

        self.canvas = Canvas(self.frame_desk, state=self.state)
        self.canvas.place(relx=0.5, rely=0.5, relwidth=1, relheight=1, anchor='center')
        self.label_image = ttk.Label(self.canvas)
        self.label_image.bind('<Double-Button-1>', self.clickOnImage)
        self.label_image.config(borderwidth=0, padding=0)
        self.window_id = self.canvas.create_window(300, 240, window=self.label_image)
        self.canvas.bind("<MouseWheel>", self.zooming)
        self.canvas.bind('<ButtonPress-1>', lambda event: self.canvas.scan_mark(event.x, event.y))
        self.canvas.bind("<B1-Motion>", lambda event: self.canvas.scan_dragto(event.x, event.y, gain=1))
        self.label_image.bind("<MouseWheel>", self.zooming)
        self.label_image.bind('<ButtonPress-1>', lambda event: self.canvas.scan_mark(event.x, event.y))
        self.label_image.bind("<B1-Motion>", lambda event: self.canvas.scan_dragto(event.x, event.y, gain=1))

        self.button_next = ttk.Button(self.frame_desk, text='>', command=self.nextSlice)
        self.button_back = ttk.Button(self.frame_desk, text='<', command=self.backSlice)
        self.entry_slicenumber = ttk.Entry(self.frame_desk, textvariable=self.slice_number, width=3)
        self.entry_slicenumber.bind('<Return>', self.sliceByNumber)
        self.frame_options = ttk.Frame(self.frame_desk)
        self.button_next.place(relx=1, rely=1, x=-5, y=-5, width=20, height=20, anchor='se')
        self.button_back.place(relx=0, rely=1, x=5, y=-5, width=20, height=20, anchor='sw')
        self.entry_slicenumber.place(relx=0.5, rely=1, y=-5, anchor='s')
        self.frame_options.place(relx=0, rely=0, x=5, y=5)

        self.combobox_view = ttk.Combobox(self.frame_options, textvariable=self.image_view, width=7)
        self.combobox_view.config(values=('axial', 'coronal', 'sagital'))
        self.combobox_view.bind('<<ComboboxSelected>>', self.comboboxView)
        self.combobox_view.pack(side=LEFT)

        self.combobox_type = ttk.Combobox(self.frame_options, textvariable=self.image_type, width=7)
        self.combobox_type.config(values=('dicom', 'binary', 'mask'))
        self.combobox_type.bind('<<ComboboxSelected>>', self.comboboxType)
        self.combobox_type.pack(side=LEFT)

        self.combobox_color = ttk.Combobox(self.frame_options, textvariable=self.image_color, width=10)
        self.combobox_color.config(values=('original', 'inverse'))
        self.combobox_color.bind('<<ComboboxSelected>>', self.comboboxColor)
        self.combobox_color.pack(side=LEFT)

        self.spinbox_threshold = ttk.Spinbox(self.frame_options, textvariable=self.image_threshold, width=5, from_=0.0,
                                             to=1.0, increment=0.1, command=self.spinboxThreshold)
        self.spinbox_threshold.bind('<Return>', self.spinboxThresholdE)
        self.spinbox_threshold.pack(side=LEFT)

        self.button_reset = ttk.Button(self.frame_desk, text='reset', command=self.resetImage)
        self.button_reset.place(relx=1, rely=0, x=-5, y=5, anchor='ne')

        self.frame_navbar = ttk.Frame(self.frame_info)
        self.button_open = ttk.Button(self.frame_navbar, text='Open', command=self.open)
        self.button_save = ttk.Button(self.frame_navbar, text='Save', command=self.save)
        self.text_info = Text(self.frame_info, width=20, height=10, state='disabled')

        self.frame_navbar.pack(padx=5, pady=5)
        self.button_open.pack(side=LEFT)
        self.button_save.pack(side=LEFT)
        self.text_info.pack(fill=BOTH, expand=True)

        self.setState(False)

        return

    def updateImage(self, arr):
        self.last_output = arr
        xyz = arr.shape
        nx = int(xyz[0] * self.zoom_level)
        ny = int(xyz[1] * self.zoom_level)
        arr = cv2.resize(arr, dsize=(ny, nx), interpolation=cv2.INTER_NEAREST)
        img = ImageTk.PhotoImage(Image.fromarray((arr * 255).astype(np.uint8)))
        self.label_image.configure(image=img)
        self.label_image.image = img
        return

    def nextSlice(self):
        output = self.dicom.changeSlice('next')
        self.updateImage(output)
        self.slice_number.set(self.dicom.current_slice)
        return

    def backSlice(self):
        output = self.dicom.changeSlice('back')
        self.updateImage(output)
        self.slice_number.set(self.dicom.current_slice)
        return

    def sliceByNumber(self, e):
        output = self.dicom.changeSlice(self.slice_number.get())
        self.updateImage(output)
        self.slice_number.set(self.dicom.current_slice)
        return

    def comboboxView(self, e):
        output = self.dicom.changeView(self.image_view.get())
        self.updateImage(output)
        self.slice_number.set(self.dicom.current_slice)
        return

    def comboboxType(self, e):
        output = self.dicom.changeType(self.image_type.get())
        self.updateImage(output)
        return

    def comboboxColor(self, e):
        output = self.dicom.changeColor(self.image_color.get())
        self.updateImage(output)
        return

    def spinboxThreshold(self):
        output = self.dicom.changeThreshold(self.image_threshold.get())
        self.updateImage(output)
        return

    def spinboxThresholdE(self, e):
        output = self.dicom.changeThreshold(self.image_threshold.get())
        self.updateImage(output)
        return

    def clickOnImage(self, e):
        x = int(e.x / self.zoom_level)
        y = int(e.y / self.zoom_level)
        output = self.dicom.selectSegment([self.slice_number.get(), y, x])
        self.updateImage(output)
        self.resetInfo()
        return

    def zooming(self, e):
        if e.delta > 0 and self.zoom_level < 2:
            self.zoom_level = round(self.zoom_level + 0.1, 1)
        elif e.delta > 0 and self.zoom_level < 10:
            self.zoom_level = round(self.zoom_level + 1, 1)
        elif e.delta < 0 and self.zoom_level > 2:
            self.zoom_level = round(self.zoom_level - 1, 1)
        elif e.delta < 0 and self.zoom_level > 0.1:
            self.zoom_level = round(self.zoom_level - 0.1, 1)
        self.updateImage(self.last_output)
        return

    def resetImage(self):
        self.zoom_level = 1
        self.updateImage(self.last_output)
        return

    def setState(self, state):
        self.state = state
        if state:
            self.canvas.configure(state='normal')
            self.button_next.configure(state='normal')
            self.button_back.configure(state='normal')
            self.entry_slicenumber.configure(state='normal')
            self.combobox_view.configure(state='normal')
            self.combobox_type.configure(state='normal')
            self.spinbox_threshold.configure(state='normal')
            self.button_reset.configure(state='disable')
        else:
            self.canvas.configure(state='disable')
            self.button_next.configure(state='disable')
            self.button_back.configure(state='disable')
            self.entry_slicenumber.configure(state='disable')
            self.combobox_view.configure(state='disable')
            self.combobox_type.configure(state='disable')
            self.spinbox_threshold.configure(state='disable')
            self.button_reset.configure(state='disable')
        return

    def resetInfo(self):
        info = self.dicom.getInfo()
        self.text_info.config(state='normal')
        self.text_info.delete('1.0', 'end')
        self.text_info.insert('1.0', info)
        self.text_info.config(state='disable')
        return

    def open(self):
        if self.state:
            self.save()
        file = filedialog.askdirectory()
        if file:
            try:
                self.file_path = file
                output = self.dicom.open(self.file_path)
                self.updateImage(output)
                self.image_view.set(self.dicom.current_view)
                self.image_threshold.set(self.dicom.current_threshold)
                self.image_type.set(self.dicom.current_type)
                self.image_color.set(self.dicom.current_color)
                self.resetImage()
                self.resetInfo()
                self.setState(True)
                messagebox.showinfo(title="Open", message='Opening was successfull.')
            except:
                messagebox.showerror(title="Can't Open", message='File is not suitable!')
        return

    def save(self):
        if self.state:
            save = messagebox.askyesno(title='Save', message='Do you want to save file operation?')
            if save:
                try:
                    self.dicom.save()
                    messagebox.showinfo(title="Save", message='Saving was successfull.')
                except:
                    messagebox.showerror(title="Can't Save", message='Saving is not applicable!')
        else:
            messagebox.showinfo(title="Save", message='There is nothing for saving!')
        return


def main():
    root = Tk()
    plt_app = PltApp(root)
    root.mainloop()


if __name__ == "__main__":
    main()
