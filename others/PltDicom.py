import scipy.io
import os
import queue
import timeit
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
import random
import numpy as np
import scipy.io
import json

class Dicom:

    def __init__(self):
        self.path = None
        self.data = None
        self.dicom = None
        self.dicom_min = None
        self.dicom_max = None
        self.dicom_size = None
        self.binary_dicom = None
        self.buffer_binary = None
        self.current_threshold = None
        self.current_view = None
        self.current_type = None
        self.current_slice = None
        self.current_frame = None
        self.anomal_segments = None

    def calculateThreshold(self,level):
        return int((level/100)*(self.dicom_max - self.dicom_min) + self.dicom_min)

    def changeThreshold(self,threshold):
        self.current_threshold = threshold
        self.buffer_binary = self.data['volume'][self.current_threshold]
        return self.refreshFrame()

    def changeView(self,view):
        self.current_view = view
        self.current_slice = 0
        return self.refreshFrame()

    def rotation(self,volume):
        if self.current_view == 'coronal':
            result = np.rot90(volume,1,(0,1))
        elif self.current_view == 'sagital':
            result = np.rot90(np.rot90(volume,1,(2,0)),1,(1,2))
        else:
            result = volume
        return result

    def changeType(self,type):
        self.current_type = type
        return self.refreshFrame()

    def changeSlice(self,direction):
        if direction == 'next':
            if self.current_view == 'axial' and self.current_slice < self.dicom_size[0]-1:
                self.current_slice +=1
            elif self.current_view == 'coronal' and self.current_slice < self.dicom_size[1]-1:
                self.current_slice +=1
            elif self.current_view == 'sagital' and self.current_slice < self.dicom_size[2]-1:
                self.current_slice +=1
        elif direction == 'back':
            if self.current_slice > 0:
                self.current_slice -=1
        elif type(direction) == int:
            self.current_slice = direction
            if self.current_view == 'axial' and self.current_slice > self.dicom_size[0]-1:
                self.current_slice = self.dicom_size[0]-1
            elif self.current_view == 'coronal' and self.current_slice > self.dicom_size[1]-1:
                self.current_slice = self.dicom_size[1]-1
            elif self.current_view == 'sagital' and self.current_slice > self.dicom_size[2]-1:
                self.current_slice = self.dicom_size[2]-1
            elif self.current_slice < 0:
                self.current_slice = 0
        return self.refreshFrame()

    def refreshFrame(self):
        if self.current_type == 'dicom':
            dicom = self.dicom
            volume = self.rotation(dicom)
            slice = volume[self.current_slice]
            slice_max = np.max(slice)
            slice_min = np.min(slice)
            slice_shape = slice.shape
            frame = np.zeros((slice_shape[0],slice_shape[1],3))
            for i in range(0,slice_shape[0]):
                for j in range(slice_shape[1]):
                    v = ((slice[i][j]-slice_min)/(slice_max-slice_min))
                    frame[i][j] = [v,v,v]
        elif self.current_type == 'binary':
            binary = self.buffer_binary
            volume = self.rotation(binary)
            slice = volume[self.current_slice]
            slice_shape = slice.shape
            frame = np.zeros((slice_shape[0],slice_shape[1],3))
            for i in range(0,slice_shape[0]):
                for j in range(0,slice_shape[1]):
                    if slice[i][j] == 0:
                        frame[i][j] = [0,0,0]
                    else:
                        if slice[i][j] in self.anomal_segments[self.current_threshold]:
                            frame[i][j] = [1,0,0]
                        else:
                            frame[i][j] = [1,1,1]
        elif self.current_type == 'mask':
            dicom = self.dicom
            binary = self.buffer_binary
            dicom_volume = self.rotation(dicom)
            binary_volume = self.rotation(binary)
            dicom_slice = dicom_volume[self.current_slice]
            slice_max = np.max(dicom_slice)
            slice_min = np.min(dicom_slice)
            binary_slice = binary_volume[self.current_slice]
            slice_shape = binary_slice.shape
            frame = np.zeros((slice_shape[0],slice_shape[1],3))
            for i in range(0,slice_shape[0]):
                for j in range(0,slice_shape[1]):
                    if binary_slice[i][j] == 0:
                        val = ((dicom_slice[i][j]-slice_min)/(slice_max-slice_min))
                        frame[i][j] = [val,val,val]
                    else :
                        if binary_slice[i][j] in self.anomal_segments[self.current_threshold]:
                            frame[i][j][0] = 1
                            frame[i][j][1] = 0
                            frame[i][j][2] = 0
                        else:
                            frame[i][j][0] = 0
                            frame[i][j][1] = 1
                            frame[i][j][2] = 0
        return frame

    def selectSegment(self,coordinate):
        if self.current_view == 'coronal':
            original_coordinate = [coordinate[1],-(coordinate[0]+1), coordinate[2]]
        elif self.current_view == 'sagital':
            original_coordinate = [coordinate[1],coordinate[2],coordinate[0]]
        else:
            original_coordinate = coordinate
        volume = self.buffer_binary
        segment_number = int(volume[original_coordinate[0]][original_coordinate[1]][original_coordinate[2]])
        if segment_number == 0:
            pass
        elif segment_number in self.anomal_segments[self.current_threshold]:
            self.anomal_segments[self.current_threshold].remove(segment_number)
        else:
            self.anomal_segments[self.current_threshold].append(segment_number)
        return self.refreshFrame()


    def save(self):
        with open(self.path+'_anomal_segments.json', 'w') as f:
            json.dump(self.anomal_segments, f)


    def open(self, path):
        self.path = path
        self.data = np.load(self.path,allow_pickle=True)
        self.dicom = self.data['dicom']
        self.dicom_min = self.data['dicom_min']
        self.dicom_max = self.data['dicom_max']
        self.dicom_size = self.dicom.shape
        self.binary_dicom = np.zeros(self.dicom_size)
        self.buffer_binary = self.data['volume'][0]
        self.current_threshold = 0
        self.current_view = 'axial'
        self.current_type = 'dicom'
        self.current_slice = 0
        self.current_frame = []
        try:
            with open(self.path+'_anomal_segments.json', 'r') as f:
                self.anomal_segments = json.load(f)
                for key, value in self.anomal_segments.items():
                    self.anomal_segments[int(key)] = self.anomal_segments.pop(key)
        except:
            self.anomal_segments = {}
            for i in range(0,11):
                self.anomal_segments[i] = list()
        return self.refreshFrame()

    def getInfo(self):
        string = ''
        keys = list(self.anomal_segments.keys())
        keys.sort()
        for k in keys:
            string += '{}%: '.format(k*10)
            for i in self.anomal_segments[k]:
                string += '{}, '.format(i)
            string += '\n'
        return string


