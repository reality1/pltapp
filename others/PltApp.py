#in the name of ALLAH
#programmer: Ali Salimi
#name: PLT Application
#description: Creating PLT application using prescripted Dicom class

from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
from PltDicom import Dicom
from PIL import Image, ImageTk
import numpy as np
import cv2

class PltApp:
    def __init__(self, master):

        self.dicom = Dicom()
        self.slice_number = IntVar()
        self.image_view = StringVar()
        self.image_type = StringVar()
        self.image_threshold = IntVar()
        self.zoom_level = 1.0
        self.last_output = None
        self.state = None
        
        master.title('PLT Application')
        master.geometry('900x480+200+200')
        
        self.panedwindow = ttk.Panedwindow(master, orient = HORIZONTAL)
        self.frame_desk = ttk.Frame(master, relief = SUNKEN)
        self.frame_info = ttk.Frame(master, relief = SUNKEN)
        self.panedwindow.pack(fill = BOTH, expand = True)
        self.panedwindow.add(self.frame_desk, weight = 4)
        self.panedwindow.add(self.frame_info, weight = 1)

        self.canvas = Canvas(self.frame_desk, state = self.state)
        self.canvas.place(relx = 0.5, rely = 0.5, relwidth = 1, relheight = 1, anchor = 'center')
        self.label_image = ttk.Label(self.canvas)
        self.label_image.bind('<Double-Button-1>', self.clickOnImage)
        self.label_image.config(borderwidth = 0, padding = 0)
        self.window_id = self.canvas.create_window(300, 240, window = self.label_image)
        self.canvas.bind("<MouseWheel>", self.zooming)
        self.canvas.bind('<ButtonPress-1>', lambda event: self.canvas.scan_mark(event.x, event.y))
        self.canvas.bind("<B1-Motion>", lambda event: self.canvas.scan_dragto(event.x, event.y, gain=1))
        self.label_image.bind("<MouseWheel>", self.zooming)
        self.label_image.bind('<ButtonPress-1>', lambda event: self.canvas.scan_mark(event.x, event.y))
        self.label_image.bind("<B1-Motion>", lambda event: self.canvas.scan_dragto(event.x, event.y, gain=1))
        
        self.button_next = ttk.Button(self.frame_desk, text = '>', command = self.nextSlice)
        self.button_back = ttk.Button(self.frame_desk, text = '<', command = self.backSlice)
        self.entry_slicenumber = ttk.Entry(self.frame_desk, textvariable = self.slice_number, width = 3)
        self.entry_slicenumber.bind('<Return>', self.sliceByNumber)
        self.frame_options = ttk.Frame(self.frame_desk)
        self.button_next.place(relx = 1, rely = 1, x = -5, y = -5, width = 20, height = 20, anchor = 'se')
        self.button_back.place(relx = 0, rely = 1, x = 5, y = -5, width = 20, height = 20, anchor = 'sw')
        self.entry_slicenumber.place(relx = 0.5, rely = 1, y = -5, anchor = 's')
        self.frame_options.place(relx = 0, rely = 0, x = 5, y = 5)

        self.combobox_view = ttk.Combobox(self.frame_options, textvariable = self.image_view, width = 7)
        self.combobox_view.config(values = ('axial', 'coronal', 'sagital'))
        self.combobox_view.bind('<<ComboboxSelected>>', self.comboboxView)
        self.combobox_view.pack(side = LEFT)

        self.combobox_type = ttk.Combobox(self.frame_options, textvariable = self.image_type, width = 7)
        self.combobox_type.config(values = ('dicom', 'binary', 'mask'))
        self.combobox_type.bind('<<ComboboxSelected>>', self.comboboxType)
        self.combobox_type.pack(side = LEFT)

        self.combobox_threshold = ttk.Combobox(self.frame_options, textvariable = self.image_threshold, width = 3)
        self.combobox_threshold.config(values = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
        self.combobox_threshold.bind('<<ComboboxSelected>>', self.comboboxThreshold)
        self.combobox_threshold.pack(side = LEFT)

        self.button_reset = ttk.Button(self.frame_desk, text = 'reset', command = self.resetImage)
        self.button_reset.place(relx = 1, rely = 0, x = -5, y = 5, anchor = 'ne')

        self.frame_navbar = ttk.Frame(self.frame_info)
        self.button_open = ttk.Button(self.frame_navbar, text = 'Open', command = self.open)
        self.button_save = ttk.Button(self.frame_navbar, text = 'Save', command = self.save)
        self.text_info = Text(self.frame_info, width = 20, height = 10, state = 'disabled')

        self.frame_navbar.pack(padx = 5, pady = 5)
        self.button_open.pack(side = LEFT)
        self.button_save.pack(side = LEFT)
        self.text_info.pack(fill = BOTH, expand = True)

        self.setState(False)

        ##ttk.Frame(master).place(relx = 0, rely = 0, relwidth = 1, relheight = 1)
        return

    def updateImage(self, arr):
        self.last_output = arr
        xyz = arr.shape
        nx = int(xyz[0] * self.zoom_level)
        ny = int(xyz[1] * self.zoom_level)
        arr = cv2.resize(arr, dsize=(ny , nx), interpolation=cv2.INTER_NEAREST)
        img = ImageTk.PhotoImage(Image.fromarray((arr * 255).astype(np.uint8)))
        self.label_image.configure(image = img)
        self.label_image.image = img
        return

    def nextSlice(self):
        output = self.dicom.changeSlice('next')
        self.updateImage(output)
        self.slice_number.set(self.dicom.current_slice)
        return

    def backSlice(self):
        output = self.dicom.changeSlice('back')
        self.updateImage(output)
        self.slice_number.set(self.dicom.current_slice)
        return

    def sliceByNumber(self, e):
        output = self.dicom.changeSlice(self.slice_number.get())
        self.updateImage(output)
        self.slice_number.set(self.dicom.current_slice)
        return 

    def comboboxView(self, e):
        output = self.dicom.changeView(self.image_view.get())
        self.updateImage(output)
        self.slice_number.set(self.dicom.current_slice)
        return

    def comboboxType(self, e):
        output = self.dicom.changeType(self.image_type.get())
        self.updateImage(output)
        return

    def comboboxThreshold(self, e):
        output = self.dicom.changeThreshold(self.image_threshold.get())
        self.updateImage(output)
        return

    def clickOnImage(self, e):
        x = int(e.x / self.zoom_level)
        y = int(e.y / self.zoom_level)
        output = self.dicom.selectSegment([self.slice_number.get(), y, x])
        self.updateImage(output)
        self.resetInfo()
        return

    def zooming(self, e):
        if e.delta > 0 and self.zoom_level < 2:
            self.zoom_level = round(self.zoom_level + 0.1, 1)
        elif e.delta > 0 and self.zoom_level < 10:
            self.zoom_level = round(self.zoom_level + 1, 1)
        elif e.delta < 0 and self.zoom_level > 2:
            self.zoom_level = round(self.zoom_level - 1, 1)
        elif e.delta < 0 and self.zoom_level > 0.1:
            self.zoom_level = round(self.zoom_level - 0.1, 1)
        self.updateImage(self.last_output)
        return

    def resetImage(self):
        self.zoom_level = 1
        self.updateImage(self.last_output)
        return

    def setState(self, state):
        self.state = state
        if state:
            self.canvas.configure(state = 'normal')
            self.button_next.configure(state = 'normal')
            self.button_back.configure(state = 'normal')
            self.entry_slicenumber.configure(state = 'normal')
            self.combobox_view.configure(state = 'normal')
            self.combobox_type.configure(state = 'normal')
            self.combobox_threshold.configure(state = 'normal')
            self.button_reset.configure(state = 'normal')
        else:
            self.canvas.configure(state = 'disable')
            self.button_next.configure(state = 'disable')
            self.button_back.configure(state = 'disable')
            self.entry_slicenumber.configure(state = 'disable')
            self.combobox_view.configure(state = 'disable')
            self.combobox_type.configure(state = 'disable')
            self.combobox_threshold.configure(state = 'disable')
            self.button_reset.configure(state = 'disable')
        return

    def resetInfo(self):
        info = self.dicom.getInfo()
        self.text_info.config(state = 'normal')
        self.text_info.delete('1.0', 'end')
        self.text_info.insert('1.0', info)
        self.text_info.config(state = 'disable')
        return

    def open(self):
        if self.state:
            self.save()
        file = filedialog.askopenfile(filetypes = [('numpy zip', '*.npz')])
        if file:
            try:
                self.file_path = file.name
                output = self.dicom.open(self.file_path)
                self.updateImage(output)
                self.image_view.set(self.dicom.current_view)
                self.image_threshold.set(self.dicom.current_threshold)
                self.image_type.set(self.dicom.current_type)
                self.resetImage()
                self.resetInfo()
                self.setState(True)
                messagebox.showinfo(title = "Open", message = 'Opening was successfull.')
            except:
                messagebox.showerror(title = "Can't Open", message = 'File is not suitable!')
        return

    def save(self):
        if self.state:
            save = messagebox.askyesno(title = 'Save', message = 'Do you want to save file operation?')
            if save:
                try:
                    self.dicom.save()
                    messagebox.showinfo(title = "Save", message = 'Saving was successfull.')
                except:
                    messagebox.showerror(title = "Can't Save", message = 'Saving is not applicable!')  
        else:
            messagebox.showinfo(title = "Save", message = 'There is nothing for saving!')
        return
        

def main():
    root = Tk()
    plt_app = PltApp(root)
    root.mainloop()

if __name__ == "__main__": main()
